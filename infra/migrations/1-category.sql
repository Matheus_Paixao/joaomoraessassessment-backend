CREATE TABLE IF NOT EXISTS `category` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL ,
    `code` VARCHAR(255) NOT NULL ,
    `slug` VARCHAR(255) NOT NULL ,
    PRIMARY KEY (`id`),
    INDEX `id_index` (`id`),
    INDEX `name_index` (`name`),
    INDEX `id_slug_name_index` (`id`, `slug`, `name`)
) ENGINE = InnoDB;