CREATE TABLE IF NOT EXISTS `product` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL ,
    `sku` VARCHAR(255) NOT NULL ,
    `price` FLOAT NOT NULL ,
    `quantity` INT(5) NOT NULL ,
    `available` TINYINT(1) NOT NULL DEFAULT '1',
    `description` TEXT NULL ,
    `slug` VARCHAR(255) NOT NULL ,
    PRIMARY KEY (`id`),
    UNIQUE `sku_unique` (`sku`),
    INDEX `id_index` (`id`),
    INDEX `sku_index` (`sku`),
    INDEX `id_sku_index` (`id`, `sku`),
    INDEX `id_slug_name_index` (`id`, `slug`, `name`)
) ENGINE = InnoDB;