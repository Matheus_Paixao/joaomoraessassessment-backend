ALTER TABLE `productCategory`
    ADD CONSTRAINT `fk_category`
    FOREIGN KEY (`category_id`)
    REFERENCES `category`(`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT;