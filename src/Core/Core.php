<?php
namespace ECommerce\Core
{
    use ECommerce\Core\Interfaces\ICore;
    use FcPhp\Di\Interfaces\IDi;
    use FcPhp\Context\Interfaces\IContext;

    class Core implements ICore
    {
        private static $instance;
        private $di;
        private $classes = [
            'FcPhp/Provider/Provider' => 'FcPhp\Provider\Provider',
            'FcPhp/Autoload/Autoload' => 'FcPhp\Autoload\Autoload',
            'FcPhp/Crypto/Crypto' => 'FcPhp\Crypto\Crypto',
        ];
        private $provider;

        public function __construct(IDi $di)
        {
            $this->di = $di;
        }

        public function load() :ICore
        {
            foreach($this->classes as $alias => $namespace) {
                $this->di->set($alias, $namespace);
            }
            return $this;
        }

        public function run()
        {
            $this->di->set('FcPhp/Context', 'FcPhp\Context\Context', ['context' => require(ECOMMERCE_CONTEXT)]);

            $context = $this->di->make('FcPhp/Context');

            $this->di->set('FcPhp/Log', 'FcPhp\Log\Log', [
                'directory' => LOGS_PATH,
                'dateFormat' => 'Y-m-d H:i:s',
                'extension' => 'log',
                'debug' => true,
                'permission' => true
            ]);

            // Make cache
            $this->di->set('FcPhp/Cache', 'FcPhp\Cache\Cache', [
                'path' => CACHE_PATH
            ]);
            

            // Make provider
            $this->provider = $this->di->make('FcPhp/Provider/Provider', [
                'di' => $this->di,
                'autoload' => $this->di->get('FcPhp/Autoload/Autoload'),
                'cache' => $this->di->get('FcPhp/Cache'),
                'vendorPath' => PROVIDER_AUTOLOAD_PATH,
                'noCache' => true
            ]);
            $this->provider->make();

            $request = $this->di->make('FcPhp/Request');

            // Console
            if ($request->isConsole()) {
                
                $command = $this->di->make('FcPhp/Command');

                $params = $request->get('params');
                unset($params[0]);
                $match = $command->match($params);

                if($match->getStatusCode() == 200) {
                    $this->di->make('ECommerce/Console')->run($match, $request->get('path'));
                } else {
                    echo __('Command not found!');
                }

            } else {
            // HTTP
                // Route process
                $route = $this->di->make('FcPhp/Route');
                $match = $route->match($_SERVER['REQUEST_METHOD'], substr($_SERVER['REQUEST_URI'], 1));

                // Verify if api call
                if($match->getStatusCode() == 200) {
                    $this->di->make('ECommerce/Api')->run($match);
                } else {
                    $this->di->make('ECommerce/Frontend')->run();
                }
            }
        }
    }
}
