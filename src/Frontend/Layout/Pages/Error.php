<script id="page-error" type="text/x-template">
    <div>
        <component-header></component-header>
        <p><?= __('Page not found'); ?></p>
        <component-footer></component-footer>
    </div>
</script>