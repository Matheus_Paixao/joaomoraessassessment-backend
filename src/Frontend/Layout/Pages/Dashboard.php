<script id="page-dashboard" type="text/x-template">
    <div>
        <component-header></component-header>
        <main class="content">
            <div class="header-list-page">
                <h1 class="title"><?= __('Dashboard'); ?></h1>
            </div>
            <div class="infor">
                <span v-if="products.length > 1 || products.length == 0"><?= __('You have %s products added on this store', '{{count}}'); ?>:</span>
                <span v-else><?= __('You have %s product added on this store', '{{products.length}}'); ?>:</span>
                <router-link to="/product/create" class="btn-action"><?= __('Add new Product'); ?></router-link>
            </div>
            <ul class="product-list">
                <li v-for="product in products">
                    <div class="product-image">
                        <router-link :to="'/product/' + product.slug" :title="product.name">
                            <img :src="product.http ? product.image : '/uploads/product/image/S/' + product.image" layout="responsive" width="164" height="145" :alt="product.name" />
                        </router-link>
                    </div>
                    <div class="product-info">
                        <div class="product-name">
                            <router-link :to="'/product/' + product.slug" :title="product.name"><span>{{ product.name }}</span></router-link>
                        </div>
                        <div class="product-price">
                            <router-link :to="'/product/' + product.slug" :title="product.name">
                                <span class="special-price" v-if="product.quantity > 0"><?= __('%s available', '{{product.quantity}}'); ?></span> 
                                <span class="special-price" v-else><?= __('Out of stock'); ?></span> 
                                <span>{{ product.price | currency('<?= __('US$'); ?>', '<?= __('en-us'); ?>') }}</span>
                            </router-link>
                        </div>
                    </div>
                </li>
            </ul>
            <paginate
              v-if="pages > 1"
              :page-count="pages"
              :click-handler="changePage"
              :prev-text="'<?= __('Prev') ?>'"
              :next-text="'<?= __('Next') ?>'"
              :container-class="'pagination'">
            </paginate>
        </main>
        <component-footer></component-footer>
    </div>
</script>