<script id="page-products" type="text/x-template">
    <div>
        <component-header></component-header>
        <modal-confirm v-show="showModalConfirm">
            <h3 slot="header"><?= __('Confirm delete product?'); ?></h3>
            <p slot="body"><?= __('Product: %s', '{{remove.name}}'); ?></p>
            <p slot="body"><?= __('This action can not be undone'); ?></p>
            <button class="button-cancel" slot="cancel" @click="showModalConfirm = false"><?= __('Cancel'); ?></button>
            <button class="button-confirm" slot="confirm" @click="deleteProductAction"><?= __('Confirm'); ?></button>
        </modal-confirm>
        <main class="content">
            <div class="header-list-page">
                <h1 class="title"><?= __('Products'); ?></h1>
                <router-link to="/product/create" class="btn-action"><?= __('Add new product'); ?></router-link>
            </div>
            <table class="data-grid">
                <tr class="data-row">
                    <th class="data-grid-th"><span class="data-grid-cell-content"><?= __('Name'); ?></span></th>
                    <th class="data-grid-th"><span class="data-grid-cell-content"><?= __('SKU'); ?></span></th>
                    <th class="data-grid-th"><span class="data-grid-cell-content"><?= __('Price'); ?></span></th>
                    <th class="data-grid-th"><span class="data-grid-cell-content"><?= __('Quantity'); ?></span></th>
                    <th class="data-grid-th"><span class="data-grid-cell-content"><?= __('Categories'); ?></span></th>
                    <th class="data-grid-th"><span class="data-grid-cell-content"><?= __('Actions'); ?></span></th>
                </tr>
                <tr class="data-row" v-for="product in products">
                    <td class="data-grid-td"><span class="data-grid-cell-content">{{ product.name }}</span></td>
                    <td class="data-grid-td"><span class="data-grid-cell-content">{{ product.sku }}</span></td>
                    <td class="data-grid-td"><span class="data-grid-cell-content">{{ product.price | currency('<?= __('US$'); ?>', '<?= __('en-us'); ?>') }}</span></td>
                    <td class="data-grid-td"><span class="data-grid-cell-content">{{ product.quantity }}</span></td>
                    <td class="data-grid-td"><span class="data-grid-cell-content" v-for="category in product.categories">{{ category.name }}<br/></span></td>
                    <td class="data-grid-td">
                        <div class="actions">
                            <div class="action edit">
                                <router-link :to="'/product/edit/' + product.slug"><span><?= __('Edit'); ?></span>
                            </router-link></div>
                            <div class="action delete">
                                <a href="javascript:;" @click="deleteProduct(product)"><span>Delete</span></a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <paginate
              v-if="pages > 1"
              :page-count="pages"
              :click-handler="changePage"
              :prev-text="'<?= __('Prev') ?>'"
              :next-text="'<?= __('Next') ?>'"
              :container-class="'pagination'">
            </paginate>
        </main>
        <component-footer></component-footer>
    </div>
</script>