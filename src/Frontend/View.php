<?php

namespace ECommerce\Frontend
{
    use ECommerce\Frontend\Interfaces\IView;

    class View implements IView
    {
        private $path;

        public function __construct(string $path)
        {
            $this->path = $path;
        }

        public function render(string $layout) :void
        {
            require($layout . '.php');
        }

        public function vueTemplate(array $templates) :string
        {
            $return = [];
            foreach($templates as $template) {
                ob_start();
                include($this->path . 'Layout/' . $template . '.php');
                $return[] = ob_get_contents();
                ob_end_clean();
            }
            return implode($return);
        }
    }
}
