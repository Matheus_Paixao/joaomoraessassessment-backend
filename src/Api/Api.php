<?php

namespace ECommerce\Api
{
    use Exception;
    use FcPhp\Di\Interfaces\IDi;
    use ECommerce\Api\Interfaces\IView;
    use FcPhp\Route\Interfaces\IEntity;
    use ECommerce\Api\Traits\Flow;

    class Api
    {
        use Flow;

        private $view;
        private $di;

        public function __construct(IDi $di, IView $view)
        {
            $this->di = $di;
            $this->view = $view;
        }

        public function run(IEntity $match)
        {
            $params = $match->getParams();
            $classInfo = explode('@', $match->getAction());
            $service = current($classInfo);
            $method = end($classInfo);

            $serviceIntance = $this->getService($service);

            if (!method_exists($serviceIntance, $method)) {
                throw new Exception('Method "' . $method . '" not found into service "' . $service . '"', 404);
            }

            if (in_array($match->getMethod(), ['POST','PUT'])) {
                $data = json_decode(file_get_contents("php://input"), true);
                if ($data) {
                    $serviceIntance->setData($data);
                }
                if (count($_FILES) > 0) {
                    $serviceIntance->setFiles($_FILES);
                }
            }
            if (count($_GET) > 0) {
                $serviceIntance->setQueryString($_GET);
            }

            $this->view->render(call_user_func_array([$serviceIntance, $method], $params));
        }
    }
}
