<?php

namespace ECommerce\Api\Datasources
{
    use PDO;
    use PDOException;
    use ECommerce\Api\Interfaces\Datasources\IDatasource;

    class MySQL implements IDatasource
    {
        private $host;
        private $port;
        private $database;
        private $username;
        private $password;
        private $connected = false;
        private $connection;

        public function __construct(string $host, int $port, string $database, string $username, string $password = null)
        {
            $this->host = $host;
            $this->port = $port;
            $this->database = $database;
            $this->username = $username;
            $this->password = $password;
        }

        public function select(string $command, array $values = []) :array
        {
            $response = [];
            $this->connect();
            $stmt = $this->connection->prepare($command);
            $stmt->execute($values);
            while ($data = $stmt->fetch()) {
                $response[] = $data;
            }
            return $response;
        }

        public function insert(string $command, array $values, bool $insert = true)
        {
            $this->connect();
            $response = [];
            // $this->connection->beginTransaction();
            $stmt = $this->connection->prepare($command);
            $stmt->execute($values);
            // $this->connection->rollBack();
            if ($insert) {
                $id = $this->connection->lastInsertId();
                return $id;
            }
            return true;
        }

        public function update(string $command, array $values)
        {
            return $this->insert($command, $values, false);
        }

        public function remove(string $command, array $values)
        {
            return $this->insert($command, $values, false);
        }

        public function query(string $sql)
        {
            $this->connect();
            return $this->connection->query($sql);
        }

        private function connect()
        {
            if (!$this->connected) {
                $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->database . ';charset=utf8';
                $options = [
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES   => false,
                ];
                try {
                     $this->connection = new PDO($dsn, $this->username, $this->password, $options);
                } catch (PDOException $e) {
                     throw new PDOException($e->getMessage(), (int)$e->getCode());
                }
            }
        }

        public function beginTransaction()
        {
            $this->connect();
            $this->connection->beginTransaction();
            return $this;
        }

        public function commit()
        {
            $this->connect();
            $this->connection->commit();
            return $this;
        }

        public function rollBack()
        {
            $this->connect();
            $this->connection->rollBack();
            return $this;
        }
    }
}