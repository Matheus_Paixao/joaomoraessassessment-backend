<?php

namespace ECommerce\Api
{
    use ECommerce\Api\Interfaces\IView;

    class View implements IView
    {
        public function render(array $content)
        {
            header('Content-Type: application/json');
            if (isset($content['error'])) {
                $content['data'] = [];
                $content['count'] = 0;
                header("HTTP/1.0 500 Internal Server Error");
            }else{
                $content['error'] = [];
                foreach ($content['data'] as $key => $model) {
                    $content['data'][$key] = $model->toArray();
                }
            }
            echo json_encode($content);
            exit;
        }

        public function renderImage(string $image)
        {
            if(file_exists($image)) {
                $ext = explode('.', $image);
                $ext = end($ext);
                $head = 'image/jpeg';
                if($ext == 'png') {
                    $head = 'image/png';
                }
                $contents=  file_get_contents($image);
                $expires = 14 * 60*60*24;
                header('Content-Type: ' . $head);
                header('Content-Length: ' . strlen($contents));
                header('Cache-Control: public', true);
                header('Pragma: public', true);
                header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
                echo $contents;
                exit;
            } else {
                $this->error();
            }
        }

        public function error()
        {
            header("HTTP/1.0 404 Not Found");
            exit;
        }
    }
}