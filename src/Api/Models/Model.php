<?php

namespace ECommerce\Api\Models
{
    use ECommerce\Api\Interfaces\Models\IModel;

    class Model implements IModel
    {
        protected $data = [];
        protected $fields = [];
        protected $validate = [];
        protected $create = true;
        protected $error = [];

        public function __construct()
        {
            $this->modify();
        }

        public function modify()
        {

        }

        public function isNew()
        {
            return $this->create;
        }

        public function populate($data)
        {
            foreach ($data as $key => $value) {
                if (in_array($key, $this->fields)) {
                    if ($key == 'id') {
                        $this->create = false;
                    }
                    $this->data[$key] = $value;
                }
            }
            return $this;
        }

        public function toArray()
        {
            return $this->data;
        }

        public function __call(string $method, array $args)
        {
            $ort = strtolower(substr($method, 0, 3));
            $prop = strtolower(substr($method, 3));
            if (in_array($prop, $this->fields)) {
                if ($ort == 'set') {
                    $this->data[$prop] = current($args);
                    return $this;
                } else {
                    if (isset($this->data[$prop])) {
                        return $this->data[$prop];
                    }
                }
            }
        }

        public function validate()
        {
            $this->data;
            $this->error = [];
            foreach ($this->validate as $key => $rule) {
                $rule = $this->defaultRules($rule);
                if ($rule['required'] && (!isset($this->data[$key]) || empty($this->data[$key]))) {
                    $this->error[] = [
                        'key' => $key,
                        'label' => $rule['label'],
                        'rule' => $rule,
                        'message' => __('Field required')
                    ];
                } else {
                    $populate = isset($rule['populate']) ? $rule['populate'] : [];
                    if ((isset($this->data[$key]) && !empty($this->data[$key])) || count($populate) > 0) {
                        if ($rule['size'] > 0) {
                            if (isset($this->data[$key]) && strlen($this->data[$key]) > $rule['size']) {
                                $this->error[] = [
                                    'key' => $key,
                                    'label' => $rule['label'],
                                    'rule' => $rule,
                                    'message' => __('Size limit is %s', $rule['size'])
                                ];
                            }
                        }
                        if ($rule['type'] == 'int') {
                            if ($this->data[$key] != (int) $this->data[$key]) {
                                $this->error[] = [
                                    'key' => $key,
                                    'label' => $rule['label'],
                                    'rule' => $rule,
                                    'message' => __('Only numbers')
                                ];
                            } else {
                                $this->data[$key] = (int) $this->data[$key];
                            }
                        }
                        if ($rule['type'] == 'float') {
                            if (($this->data[$key] != (float) $this->data[$key] && $this->data[$key] != (int) $this->data[$key])) {
                                $this->error[] = [
                                    'key' => $key,
                                    'label' => $rule['label'],
                                    'rule' => $rule,
                                    'message' => __('Only numbers and dot (0.00)')
                                ];
                            } else {
                                $this->data[$key] = (float) $this->data[$key];
                            }
                        }
                        if ($rule['type'] == 'array') {
                            if (empty($this->data[$key])) {
                                $this->data[$key] = [];
                            }
                        }
                        switch($rule['format']) {
                            case 'slug':
                                $terms = [];
                                foreach ($populate as $item) {
                                    if (!empty($this->data[$item])) {
                                        $terms[] = $this->data[$item];
                                    }
                                }
                                $term = implode(' ', $terms);
                                if ($rule['size'] > 0) {
                                    $this->data[$key] = $this->slugify(substr($term, 0, $rule['size']));
                                } else {
                                    $this->data[$key] = $this->slugify($term);
                                }
                                break;
                            case 'html':
                                $this->data[$key] = base64_encode($this->data[$key]);
                                break;
                            case 'raw':
                                $this->data[$key] = $this->data[$key];
                                break;
                            default:
                                $this->data[$key] = addslashes($this->data[$key]);
                                break;
                        }
                    }
                }
            }
            if (count($this->error) == 0) {
                return true;
            }
            return false;
        }

        public function error()
        {
            return [
                'error' => $this->error
            ];
        }

        protected function defaultRules(array $rule)
        {
            return array_merge([
                'type' => 'string',
                'size' => null,
                'label' => null,
                'populate' => null,
                'format' => 'default',
                'required' => false
            ], $rule);
        }

        protected function slugify($text)
        {
            $text = preg_replace('~[^\pL\d]+~u', '-', $text);
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            $text = preg_replace('~[^-\w]+~', '', $text);
            $text = trim($text, '-');
            $text = preg_replace('~-+~', '-', $text);
            $text = strtolower($text);
            if (empty($text)) {
                return 'n-a';
            }
            return $text;
        }
    }
}