<?php

namespace ECommerce\Api\Services
{
    use Exception;
    use FcPhp\Di\Interfaces\IDi;
    use ECommerce\Api\Traits\Flow;
    use FcPhp\Log\Interfaces\ILog;
    use ECommerce\Api\Interfaces\Services\IService;
    use ECommerce\Api\Interfaces\Models\IModel;

    class Service implements IService
    {
        use Flow;

        private $di;
        protected $data = [];
        protected $queryString = [];
        protected $files = [];

        public function __construct(IDi $di, ILog $log)
        {
            $this->di = $di;
            $this->log = $log;
        }

        public function setData(array $data) :IService
        {
            $this->data = $data;
            return $this;
        }

        public function setFiles(array $files) :IService
        {
            $this->files = $files;
            return $this;
        }

        public function setQueryString(array $queryString) :IService
        {
            $queryString = array_map(function($value) {
                return addslashes($value);
            }, $queryString);
            $this->queryString = $queryString;
            return $this;
        }

        public function data(string $key)
        {
            if (isset($this->data[$key])) {
                return $this->data[$key];
            }
            if (isset($this->files[$key])) {
                return $this->files[$key];
            }
            return null;
        }

        public function queryString(string $key)
        {
            if (isset($this->queryString[$key])) {
                return $this->queryString[$key];
            }
            return null;
        }

        protected function messageError(string $message, string $label = null)
        {
            return ['error' => [[
                'message' => $message,
                'label' => $label,
            ]]];
        }

        protected function logErrorModel(string $label, array $errors)
        {
            $message = [];
            if(count($errors) > 0) {
                foreach($errors as $error) {
                    $message[] = $error['label'] . ' ["' . $error['message'] . '"]';
                }
                // $this->log->error($label . ': ' .implode(', ', $message));
            }
        }

        public function logSuccessModel(string $label, IModel $model, IModel $modelOld = null)
        {
            $message = [];
            $diff = [];
            $data = $model->toArray();
            foreach($data as $key => $item) {
                if(is_array($item)) {
                } else {
                    $message[] = $key . '=' . $item;
                }
            }
            if ($modelOld instanceof IModel) {
                $dataOld = $modelOld->toArray();
                foreach($data as $key => $item) {
                    if (isset($dataOld[$key])) {
                        if ($dataOld[$key] != $item) {
                            $diff[] = $key . ' -["' . $dataOld[$key] . '"] +["' . $item . '"]';
                        }
                    }
                }
            }
            // $this->log->info($label . ': [' . implode(', ', $message) . '] DIFF: ' . implode(', ', $diff));
        }
    }
}