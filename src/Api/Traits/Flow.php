<?php

namespace ECommerce\Api\Traits
{
    use Exception;
    
    trait Flow
    {

        protected function getService(string $service)
        {
            return $this->getInstance($service, 'Service "%s" not found');
            
        }

        protected function getRepository(string $repository)
        {
            return $this->getInstance($repository, 'Repository "%s" not found');
            
        }

        protected function getModel(string $model)
        {
            return $this->getInstance($model, 'Model "%s" not found');
        }

        protected function getContext()
        {
            return $this->di->make('FcPhp/Context');
        }

        protected function getView()
        {
            return $this->di->make('ECommerce/Api/View');
        }

        protected function getCommand(string $command)
        {
            return $this->getInstance($command, 'Command Console "%s" not found');
        }

        private function getInstance(string $term, string $message)
        {
            if ($this->di->has($term)) {
                return $this->di->make($term);
            }
            throw new Exception(sprintf($message, $term), 404);
        }
    }
}