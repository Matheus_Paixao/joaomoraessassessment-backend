<?php

namespace ECommerce\Api\Repositories
{
    use ECommerce\Api\Interfaces\Models\IModel;

    class ProductsCategoriesRepository extends Repository
    {
        public function find(array $options = [])
        {
            $data = $this->select('
                SELECT
                    `c`.`id`,
                    `c`.`name`,
                    `c`.`code`,
                    `c`.`slug`
                FROM `productCategory` AS `pc`
                INNER JOIN `category` AS `c` ON `c`.`id` = `pc`.`category_id`
                WHERE 1
                ORDER BY `c`.`name` ASC
            ', null, $options);

            $count = $this->select('
                SELECT COUNT(`c`.`id`) AS `count`
                FROM `productCategory` AS `pc`
                INNER JOIN `category` AS `c` ON `c`.`id` = `pc`.`category_id`
                WHERE 1', null, $options, true);

            return [
                'count' => $count[0]['count'],
                'data' => $data
            ];
        }

        public function findByProduct(IModel $model)
        {
            return $this->find([
                'condition' => '`pc`.`product_id` = :product_id',
                'value' => [
                    'product_id' => $model->getProduct_Id()
                ]
            ]);
        }

        public function store(IModel $model)
        {
            $id = $this->insert('
                INSERT INTO `productCategory`
                (`product_id`,`category_id`) VALUES
                (:product_id, :category_id)', [
                    'product_id' => $model->getProduct_Id(),
                    'category_id' => $model->getCategory_Id(),
                ]);
            $model->setId($id);
        }

        public function deleteByProduct(IModel $model)
        {
            return $this->remove('
                DELETE FROM `productCategory`
                WHERE `product_id` = :product_id', [
                    'product_id' => $model->getProduct_Id()
                ]);
        }
    }
}