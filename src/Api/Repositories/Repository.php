<?php

namespace ECommerce\Api\Repositories
{
    use FcPhp\Di\Interfaces\IDi;
    use ECommerce\Api\Traits\Flow;
    use ECommerce\Api\Interfaces\Models\IModel;
    use ECommerce\Api\Interfaces\Datasources\IDatasource;

    class Repository
    {
        use Flow;

        private $di;
        protected $datasource;

        public function __construct(IDi $di, IDatasource $datasource)
        {
            $this->di = $di;
            $this->datasource = $datasource;
        }

        public function select(string $command, string $model = null, array $options = [], bool $ignorePagination = false)
        {
            $response = [];
            
            $condition = isset($options['condition']) ? $options['condition'] : null;
            $value = isset($options['value']) ? $options['value'] : [];
            $limit = isset($options['limit']) ? $options['limit'] : false;
            $offset = isset($options['offset']) ? $options['offset'] : false;
            $distinct = isset($options['distinct']) ? $options['distinct'] : false;
            $fields = isset($options['fields']) ? $options['fields'] : [];

            if ($distinct && strpos($command, 'DISTINCT') === false) {
                $command = str_replace('SELECT', 'SELECT DISTINCT ', $command);
            }

            if (count($fields) > 0) {
                $distinct = explode('DISTINCT', $command);
                $from = explode('FROM', $command);
                $command = 'SELECT ' . (count($distinct) == 2 ? 'DISTINCT' : '') . implode(',', $fields) . ' FROM' . $from[1];
            }

            if ($condition) {
                $command = str_replace('WHERE 1', 'WHERE ' . $condition, $command);
            }

            if($limit && $offset && !$ignorePagination) {
                $command .= ' LIMIT ' . (int) $offset . ',' . (int) $limit;
            } else {
                if($limit && !$ignorePagination) {
                    $command .= ' LIMIT ' . $limit;
                }
            }

            $data = $this->datasource->select($command, $value);
            if (count($data) > 0) {
                foreach ($data as $row) {
                    if(!empty($model)) {
                        $response[] = $this->getModel($model)->populate($row);
                    } else {
                        $response[] = $row;
                    }
                }
            }
            return $response;
        }

        public function insert(string $command, array $values)
        {
            return $this->datasource->insert($command, $values);
        }

        public function update(string $command, array $values)
        {
            return $this->datasource->update($command, $values);
        }

        public function remove(string $command, array $values)
        {
            return $this->datasource->remove($command, $values);
        }

        public function beginTransaction()
        {
            $this->datasource->beginTransaction();
            return $this;
        }

        public function commit()
        {
            $this->datasource->commit();
            return $this;
        }

        public function rollBack()
        {
            $this->datasource->rollBack();
            return $this;
        }
    }
}