<?php

namespace ECommerce\Console
{
    use Exception;
    use FcPhp\Di\Interfaces\IDi;
    use ECommerce\Api\Traits\Flow;
    use FcPhp\Command\Interfaces\ICEntity;
    use FcPhp\Request\Interfaces\IRequest;

    class Console
    {
        use Flow;

        private $di;

        public function __construct(IDi $di)
        {
            $this->di = $di;
        }

        public function run(ICEntity $match, string $path)
        {
            $params = $match->getParams();
            $classInfo = explode('@', $match->getAction());
            $command = current($classInfo);
            $method = end($classInfo);

            $consoleInstance = $this->getCommand($command);

            if (!method_exists($consoleInstance, $method)) {
                throw new Exception('Method "' . $method . '" not found into command "' . $command . '"', 404);
            }

            $consoleInstance->setData($params);

            call_user_func_array([$consoleInstance, $method], $params);
        }
    }
}