<?php

namespace ECommerce\Console\Command
{
    class FixIp extends Command
    {
        public function run()
        {
            $params = $this->data();
            $fileContext = current($params);
            $ip = end($params);
            if(is_file($fileContext)) {
                $content = require($fileContext);
                $content['datasource']['mysql']['host'] = $ip;
                copy($fileContext, $fileContext . '-backup-' . time());
                file_put_contents($fileContext, '<?php return ' . var_export($content, true) . ';');
            }
        }
    }
}