<?php

namespace ECommerce\Console\Command
{
    use FcPhp\Di\Interfaces\IDi;
    use ECommerce\Api\Interfaces\Datasources\IDatasource;

    class DatasourceMigrate extends Command
    {
        private $datasource;

        public function __construct(IDi $di, IDatasource $datasource)
        {
            $this->datasource = $datasource;
            parent::__construct($di);
        }

        public function run()
        {
            $files = [];
            $ignore = ['.', '..'];
            $context = $this->getContext();
            $dir = DATASOURCE_MIGRATION_PATH;
            if ($handle = opendir($dir)) {
                while (false !== ($entry = readdir($handle))) {
                    if (!in_array($entry, $ignore)) {
                        $files[$entry] = $dir . '/' . $entry;
                    }
                }
                ksort($files);
                foreach ($files as $file) {
                    $content = file_get_contents($file);
                    $this->datasource->query($content);
                    // d($content, true);
                }
            }
        }
    }
}