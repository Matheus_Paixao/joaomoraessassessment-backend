<?php

namespace ECommerce\Config\Providers
{
    use FcPhp\Di\Interfaces\IDi;
    use FcPhp\Provider\Interfaces\IProviderClient;

    class Route implements IProviderClient
    {
        /**
         * Method to configure Di in providers
         *
         * @param FcPhp\Di\Interfaces\IDi $di Di Instance
         * @return void
         */
        public function getProviders(IDi $di) :IDi
        {
            $context = $di->make('FcPhp/Context');

            $di->set('FcPhp/SHttp/SEntity', 'FcPhp\SHttp\SEntity');
            $di->set('FcPhp/Route/Factories/RouteFactory', 'FcPhp\Route\Factories\RouteFactory', compact('di'));

            $di->set('FcPhp/Route', 'FcPhp\Route\Route', [
                'entity' => $di->get('FcPhp/SHttp/SEntity', ['expires' => 86000]),
                'autoload' => $di->get('FcPhp/Autoload/Autoload'),
                'cache' => $di->get('FcPhp/Cache'),
                'vendorPath' => FRONTEND_AUTOLOAD_PATH,
                'factory' => $di->get('FcPhp/Route/Factories/RouteFactory'),
                'noCache' => true,
                'routes' => []
            ]);
            return $di;
        }
    }
}