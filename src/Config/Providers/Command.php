<?php

namespace ECommerce\Config\Providers
{
    use FcPhp\Di\Interfaces\IDi;
    use FcPhp\Provider\Interfaces\IProviderClient;

    class Command implements IProviderClient
    {
        /**
         * Method to configure Di in providers
         *
         * @param FcPhp\Di\Interfaces\IDi $di Di Instance
         * @return void
         */
        public function getProviders(IDi $di) :IDi
        {
            $context = $di->make('FcPhp/Context');

            $di->set('FcPhp/Command/Factories/Command', 'FcPhp\Command\Factories\CommandFactory', [
                'di' => $di
            ]);
            $di->set('FcPhp/SConsole/SCEntity', 'FcPhp\SConsole\SCEntity', ['expires' => 84000]);
            $di->set('FcPhp/Command', 'FcPhp\Command\Command', [
                'entity' => $di->get('FcPhp/SConsole/SCEntity'),
                'autoload' => $di->get('FcPhp/Autoload/Autoload'),
                'cache' => $di->get('FcPhp/Cache'),
                'vendorPath' => COMMAND_AUTOLOAD_PATH,
                'factory' => $di->get('FcPhp/Command/Factories/Command'),
                'commands' => [],
                'noCache' => true
            ]);

            return $di;
        }
    }
}