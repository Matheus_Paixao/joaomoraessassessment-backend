<?php

return [
    'datasource' => [
        'mysql' => [
            'host' => '',
            'port' => '3306',
            'database' => 'webjump',
            'username' => 'root',
            'password' => 'root'
        ]
    ],
    'uploadImage' => [
        'headerEnable' => [
            'image/jpg',
            'image/jpeg',
            'image/png',
        ],
        'sizes' => [
            'S' => [200,200],
            'M' => [640,640],
            'L' => [1000,1000],
        ]
    ]
];