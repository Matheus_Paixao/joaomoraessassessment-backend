.PHONY: server-install infra-down docker-down install-docker infra infra-install infra-setup var docker-up app-install composer-install npm-install server-logs install-docker-compose fix-ip migrate

VENDOR_DIR = ./vendor
COMPOSER_PHAR = ./bin/composer.phar
RELEASE :=  $(shell lsb_release -cs)
DOCKER_PHP_ID := $(shell sudo docker ps -aqf "name=serverphpnpm")
DOCKER_MYSQL_ID := $(shell sudo docker ps -aqf "name=mysqldb")
LOCAL_USER_ID := $(shell id -u)
LOCAL_GROUP_ID := $(shell id -g)
DOCKER_MYSQL_IP := $(shell sudo docker exec -it $(DOCKER_MYSQL_ID) bash -c 'hostname -i')

server-install: install-docker install-docker-compose
infra-install: var docker-up
infra-setup: fix-ip migrate var
infra: docker-up fix-ip
infra-down: docker-down
app-install: composer-install npm-install

install-docker:
	sudo apt-get remove docker docker-engine docker.io containerd runc -y; \
	sudo apt-get update; \
	sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y; \
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -; \
	sudo add-apt-repository "deb https://download.docker.com/linux/ubuntu $(RELEASE) stable"; \
	sudo apt-get update; \
	sudo apt-get install docker-ce docker-ce-cli containerd.io -y;

install-docker-compose:
	sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(shell uname -s)-$(shell uname -m)" -o /usr/local/bin/docker-compose; \
	sudo chmod +x /usr/local/bin/docker-compose

var:
	if [ ! -d "./var" ]; then \
		mkdir var; \
	fi
	chmod -R 777 var; \
	if [ ! -d "./var/cache" ]; then \
		mkdir var/cache; \
	fi
	chmod -R 777 var/cache; \
	if [ ! -d "./var/upload" ]; then \
		mkdir var/upload; \
	fi
	chmod -R 777 var/upload; \
	if [ ! -d "./var/logs" ]; then \
		mkdir var/logs; \
	fi
	chmod -R 777 var/logs; \

docker-up:
	sudo docker-compose -f infra/docker-compose.yml up -d;

docker-down:
	sudo docker-compose -f infra/docker-compose.yml stop;

composer-install:
	bash -c "sudo docker exec -it $(DOCKER_PHP_ID) bash -c 'mkdir /.composer && chown $(LOCAL_USER_ID):$(LOCAL_GROUP_ID) /.composer'"; \
	if [ ! -d $(VENDOR_DIR) ]; then \
		bash -c "sudo docker exec --user $(LOCAL_USER_ID):$(LOCAL_GROUP_ID) -it $(DOCKER_PHP_ID) bash -c 'cd /var/www/html && composer install'"; \
	else \
		bash -c "sudo docker exec --user $(LOCAL_USER_ID):$(LOCAL_GROUP_ID) -it $(DOCKER_PHP_ID) bash -c 'cd /var/www/html && composer update'"; \
	fi

npm-install:
	bash -c "sudo docker exec -it $(DOCKER_PHP_ID) bash -c 'cd /var/www/html && npm install --prefix ./public_html/assets && rm -Rf ./public_html/assets/etc && chown -R $(LOCAL_USER_ID):$(LOCAL_GROUP_ID) public_html/assets/node_modules'";
	
server-logs:
	bash -c "sudo docker exec -it $(DOCKER_PHP_ID) bash -c 'tail -f /var/log/apache2/error.log'";

fix-ip:
	bash -c "sudo docker exec -it $(DOCKER_PHP_ID) bash -c 'cd /var/www/html && bin/console datasource fix-ip src/Config/context.php $(DOCKER_MYSQL_IP)'";

migrate:
	bash -c "sudo docker exec -it $(DOCKER_PHP_ID) bash -c 'cd /var/www/html && ./bin/console datasource migrate'";

import:
	echo $0