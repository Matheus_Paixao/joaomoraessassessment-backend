var depends = [];
depends.push('vue');

define(depends, function(Vue) {
    return Vue.component('ComponentHeader', {
        template: '#component-header',
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
    });
});
