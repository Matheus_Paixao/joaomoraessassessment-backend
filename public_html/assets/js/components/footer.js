var depends = [];
depends.push('vue');
depends.push('bus');

define(depends, function(Vue) {
    return Vue.component('ComponentFooter', {
        template: '#component-footer',
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                contact: 'go@jumpers.com.br'
            };
        }
    });
});
