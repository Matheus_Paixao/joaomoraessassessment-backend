var depends = [];

depends.push('vue');
depends.push('v-money');
depends.push('vue-multiselect');
depends.push('component-header');
depends.push('component-footer');

define(depends, function(Vue, Money, VueMultiSelect) {
    return Vue.component('PageProductCreate', {
        template: '#page-product-create',
        components: {
            'money': Money.Money,
            'multiselect': VueMultiSelect.Multiselect
        },
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                categories: [],
                product: {
                    image: null,
                    name: null,
                    description: null,
                    slug: null,
                    available: false,
                    value: null,
                    sku: null,
                    quantity: null,
                    categories: []
                },
                error: {
                    name: false,
                    sku: false,
                    price: false,
                    quantity: false,
                    available: false
                },
                errors: []
            };
        },
        mounted: function() {
            var self = this;
            if (this.$route.params.slug) {
                this.request('get', 'product/' + this.$route.params.slug, function(count, data) {
                    self.product = data[0];
                });
            }
            this.request('get', 'categories', function(count, data) {
                self.categories = data;
            });
        },
        methods: {
            save: function() {
                var self = this;

                var rules = [{
                    value: this.product.name,
                    rule: 'string',
                    required: true,
                    fail: function() {
                        self.error.name = true;
                    },
                    success: function(value) {
                        self.error.name = false;
                        self.product.name = value;
                    }
                },{
                    value: this.product.sku,
                    rule: 'string',
                    required: true,
                    fail: function() {
                        self.error.sku = true;
                    },
                    success: function(value) {
                        self.error.sku = false;
                        self.product.sku = value;
                    }
                },{
                    value: this.product.price,
                    rule: 'float',
                    required: true,
                    fail: function() {
                        self.error.price = true;
                    },
                    success: function(value) {
                        self.error.price = false;
                        self.product.price = value;
                    }
                },{
                    value: this.product.quantity,
                    rule: 'int',
                    required: true,
                    fail: function() {
                        self.error.quantity = true;
                    },
                    success: function(value) {
                        self.error.quantity = false;
                        self.product.quantity = value;
                    }
                },{
                    value: this.product.description,
                    rule: 'string',
                    required: false,
                    fail: function() {
                        self.error.description = true;
                    },
                    success: function(value) {
                        self.error.description = false;
                        self.product.description = value;
                    }
                },{
                    value: this.product.available,
                    rule: 'bool',
                    required: false,
                    fail: function() {
                        self.error.available = true;
                    },
                    success: function(value) {
                        self.error.available = false;
                        self.product.available = value;
                    }
                }];

                var data = {
                    name: this.product.name,
                    sku: this.product.sku,
                    price: this.product.price,
                    quantity: this.product.quantity,
                    description: this.product.description,
                    categories: this.product.categories,
                    available: this.product.available
                };

                if (!this.validate(rules)) {
                    var slug = this.$route.params.slug;
                    var path = 'product';
                    var method = 'post';
                    if (slug) {
                        path += '/' + slug;
                        method = 'put';
                    }
                    this.request(method, path, function(count, data) {
                        if(self.product.image != undefined && self.product.image != "") {
                            if(!slug) {
                                path += '/' + data[0].slug;
                            }
                            self.requestImage(path + '/image', self.product.image, function(response) {
                                self.$router.push('/products');
                            });
                        }else{
                            self.$router.push('/products');
                        }
                    }, data);
                }
            },

            configureImage: function(event) {
                this.product.image = event.target.files[0];
            }
        }
    });
});
