var depends = [];

depends.push('vue');
depends.push('component-header');
depends.push('component-footer');
depends.push('modal-confirm');

define(depends, function(Vue) {
    return Vue.component('PageCategories', {
        template: '#page-categories',
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                count: 0,
                categories: [],
                remove: {},
                showModalConfirm: false,
                errors: []
            };
        },
        mounted: function() {
            this.populate();
        },
        methods: {
            populate: function() {
                var self = this;
                this.request('get', 'categories', function(count, data) {
                    self.count = count;
                    self.categories = data;
                });
            },
            deleteCategory: function(category) {
                this.remove = category;
                this.showModalConfirm = true;
            },
            deleteCategoryAction: function() {
                var self = this;
                this.request('delete', 'category/' + this.remove.id, function(count, data) {
                    self.showModalConfirm = false;
                    self.populate();
                });
            }
        }
    });
});