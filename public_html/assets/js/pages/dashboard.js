var depends = [];
depends.push('vue');
depends.push('vuejs-paginate');
depends.push('currency');

define(depends, function(Vue, VuejsPaginate) {

    return Vue.component('PageDashboard', {
        template: '#page-dashboard',
        components: {
            'paginate': VuejsPaginate,
        },
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                currentPage: 1,
                pages: 0,
                perPage: 20,
                count: 0,
                products: []
            };
        },
        mounted: function() {
            this.populate();
        },
        methods: {
            populate: function() {
                var self = this;
                this.request('get', 'products', function(count, data, pages) {
                    self.count = count;
                    self.products = data;
                    self.pages = pages;
                    for(var i in self.products) {
                        var product = self.products[i];
                        if (product.images.length > 0) {
                            self.products[i].image = product.images[0].name;
                            self.products[i].http = false;
                        } else {
                            self.products[i].image = 'https://via.placeholder.com/200x200';
                            self.products[i].http = true;
                        }
                    }
                }, {}, this.currentPage, this.perPage);
            },
            changePage: function(currentPage) {
                this.currentPage = currentPage;
                window.scrollTo(0,200);
                this.populate();
            }
        }
    });
});
