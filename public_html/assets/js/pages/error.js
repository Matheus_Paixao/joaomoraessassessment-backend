var depends = [];
depends.push('vue');
depends.push('component-header');
depends.push('component-footer');

define(depends, function(Vue) {
    return Vue.component('PageError', {
        template: '#page-error',
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                
            };
        }
    });
});
