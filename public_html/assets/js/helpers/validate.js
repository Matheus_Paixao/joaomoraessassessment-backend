var depends = [];
depends.push('vue');

define(depends, function(Vue) {
    Vue.mixin({
        data: function() {
            return {
                validateError: false
            };
        },
        methods: {
            validate: function(items) {
                this.validateError = false;
                for (var i in items) {
                    var item = items[i];
                    if (item.required && (item.value == "" || item.value == null)) {
                        this.validateError = true;
                        item.fail();
                    } else {
                        switch (item.rule) {
                            case 'int':
                            case 'integer':
                                item.success(parseInt(item.value));
                                break;
                            default:
                                item.success(item.value);
                                break;
                        }
                    }
                }
                return this.validateError;
            }
        }
    });
});
