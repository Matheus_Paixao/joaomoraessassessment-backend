var depends = [];
depends.push('vue');
depends.push('axios');

define(depends, function(Vue, Axios) {
    Vue.mixin({
        methods: {
            request: function(method, path, callback, data, currentPage, perPage) {
                var axios;
                var self = this;

                if (method == 'post' || method == 'put') {
                    axios = Axios[method]('/api/v1/' + path, data);
                } else {
                    axios = Axios[method]('/api/v1/' + path, {
                        params: {
                            currentPage: currentPage,
                            perPage: perPage
                        }
                    });
                }
                
                axios
                    .then(function(response) {
                        var count = response.data.count;
                        var data = response.data.data;
                        var pages = response.data.pages;
                        callback(count, data, pages);
                    })
                    .catch(function(exception) {
                        console.log(exception);
                        self.errors = exception.response.data.error;
                    });
            },

            requestImage: function(path, image, callback) {
                var form = new FormData();
                form.append('image', image)
                var axios = Axios.post('/api/v1/' + path, form);
                axios
                    .then(function(response) {
                        callback(response);
                    })
                    .catch(function(exception) {
                        console.log(exception);
                        // self.errors = exception.response.data.error;
                    });
            }
        }
    });
});
